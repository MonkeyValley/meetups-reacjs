import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter as Router, Route} from "react-router-dom";

import Login from './Views/Login.js'
import Home from './Views/Home.js'
import Register from './Views/Register.js'

const routing = (
    <Router>
        <Route exact path="/" component={Login} />
        <Route path="/home" component={Home} />
        <Route path="/register" component={Register} />        
    </Router>
  )
  ReactDOM.render(routing, document.getElementById('root'))

serviceWorker.unregister();
