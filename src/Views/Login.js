import React from 'react';
import Alert from "../Helpers/Alert"
import '../StyleSheet/login.css'
import FacebookLogin from 'react-facebook-login';

class Login extends React.Component{

    
    constructor(props){
        super()
        this.state = {
            txtCorreo:"",
            txtPassword:"",
            showAlert:false,
            typeAlert:"",
            tituloAlert:"",
            msjAlert:"",
        }

        this.hideAlert = this.hideAlert.bind(this)

        let h = localStorage.getItem('myData');
        if(h === null){
            props.history.push("/")
        }else{
            props.history.push("/home")
        }
    }
    hideAlert(){
        this.setState({showAlert:false})
    }
    componentClicked = () =>{

    }
    responseFacebook = response =>{
        let obj = {
            name:response.name,
            type:1,
            id:response.id
        }
        localStorage.setItem('myData', JSON.stringify(obj));
        this.props.history.push("/home")
    }
    login(){
        let correo = this.state.txtCorreo
        let contra = this.state.txtPassword
       
        if(correo !== "" && contra !== ""){

            let params = {
                cPassword: contra,
                cEmail: correo
            }
            fetch('http://35.229.89.115/temp/meetup/api/users/login', {
                method: 'POST',
                body: JSON.stringify(params),
                headers: {
                "Content-type": "application/json; charset=UTF-8"
                }
            })
            .then((response) => {
            return response.json()
            })
            .then((res) => {
            
            if(res.respuesta){
                localStorage.setItem('myData', JSON.stringify(res.data));
                this.props.history.push("/home")
            }else{
                this.setState({ showAlert:true, typeAlert:"error",  tituloAlert:"¡Error!", msjAlert:res.error})
            }

            })
            .catch((err) => {
            this.setState({ showAlert:true, typeAlert:"error",  tituloAlert:"¡Espera!", msjAlert:"Error al contactar el servicio"})
        })
        }else{
            this.setState({ showAlert:true, typeAlert:"warning",  tituloAlert:"¡Espera!", msjAlert:"Faltan datos"})
        }
    }

    render(){
        return (
            <div>
                
                {/* style={styles.container} */}
                <div className="container">
                <Alert type={this.state.typeAlert} show={this.state.showAlert} titulo={this.state.tituloAlert} mensaje = {this.state.msjAlert} handleClose={this.hideAlert}></Alert>                       
                {/* style={styles.form} */}
                    <div className="form" >
                        <div style={styles.divHeader}>
                            <h1 style={styles.title}>Login</h1>
                        </div>
                        {/* style={styles.divForm } */}
                        <div  className="divForm">
                            {/*style={styles.inputs}*/}
                            <input type={"email"}  className="inputs" placeholder="Correo" onChange={(evt) => {
                                let value = evt.target.value
                                this.setState({txtCorreo:value})
                            }}></input>
                            {/*style={styles.inputs} */}
                            <input type={"password"} className="inputs" placeholder="Contraseña" onChange={(evt) => {
                                let value = evt.target.value
                                this.setState({txtPassword:value})
                            }}></input>
                        </div>
                        {/* style={styles.divButtons} */}
                        <div className="divButtons" >
                            <button style={styles.btnEntrar} onClick={() => {
                                this.login()
                            }}>
                                Entrar
                            </button>
                            <div className="btnFacebook">
                            <FacebookLogin
                                
                                appId="498800247395540"
                                autoLoad={false}
                                fields="name,email,picture"
                                onClick={this.componentClicked}
                                callback={this.responseFacebook} />
                            </div>
                            {/* style={styles.btnRegister}  */}
                            <button className="btnRegister" onClick={() => {
                                 this.props.history.push("/register")
                            }}>
                                Registarse
                            </button>

                            
                        </div>
                    </div>
                    <div style={{width:"95%", background:"", textAlign:"right", padding:"20px 16px", bottom:0, position:"absolute"}}>
                        <div style={{background:"", fontSize:24}}>
                            ¿Quieres expresar tus ideas?   <button onClick={() => this.props.history.push("/register")} style={{color:"#3f51b5", fontSize:24, backgroundColor:"white", border:"none", outline:"none"}}> Contactanos </button>
                        </div>
                    </div>
                
                </div>

            </div>
        );
    }
}

const styles = {
    container:{
        position: "absolute",
        flex: 1,
        width:"100%",
        height:"100vh",

    },
    form:{
        padding:"10px 20px",
        margin:"10% auto 0 auto",
        width: "30%",
        borderRadius: 10,
        border:"0.5px solid #eee ",
        backgroundColor:"white",
        boxShadow: "10px 10px 10px rgba(0,0,0, 0.1)"

    },
    title:{
        color:"#696969",
        textAlign:"center",
    },
    inputs:{
        borderTop:"none",
        borderLeft:"none",
        borderRight:"none",
        borderBottom:"0.5px solid #eee",
        height:45,
        fontSize:19,
        outline:"none",
        width:"100%",
        marginTop: 16,
    },
    divHeader:{
        width:"100%",
    },
    divForm:{
        width:"100%",
        backgroundColor:"green"
    },
    divButtons:{
        height:"10%",
        padding: "10px 10px",
        textAlign:"right",
        marginTop:20
    },
    btnEntrar:{
        width:"100%",
        height:45,
        backgroundColor:"#bbdefb",
        borderRadius:5,
        border:"none",
        outline:"none",
        fontSize:17,
        color:"white"
    },
    
    btnRegister:{
        width:"100%",
        marginTop:16,
        borderRadius:5,
        border:"none",
        outline:"none",
        fontSize:17,
        color:"#bbdefb",
        backgroundColor:"white"
    }
}


export default Login;