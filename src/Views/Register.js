import React from 'react';
import Alert from "../Helpers/Alert"
import NavBar from '../components/navBar'
import AwesomeInput from '../components/comInput'
import '../StyleSheet/register.css'

class Login extends React.Component{

    constructor(props){
        super()
        this.state = {
            tiposUSuarios:[{texto:"Mortal", valor:"1"}, {texto:"Super user", valor:"0"}],
            txtNombre:"",
            txtCorreo:"",
            txtxContraseña:"",
            txtType:"",
            typeAlert:"",
            showAlert:false,
            tituloAlert:"",
            msjAlert:""
        }
        this.backto = this.backto.bind(this)
        this.onChangeText = this.onChangeText.bind(this)
        this.hideAlert = this.hideAlert.bind(this)
    }

    backto(){
        this.props.history.push("/")
    }

    onChangeText(value, ide){
        if(ide==="nombre"){
            this.setState({txtNombre:value})
        }else if(ide==="correo"){
            this.setState({txtCorreo:value})
        }else if(ide==="contrasena"){
            this.setState({txtxContraseña:value})
        }else if(ide==="tipo"){
            this.setState({txtType:value})
        }
    }

    hideAlert(){
        this.setState({showAlert:false})
    }
    
    registro(){
    let params = {
            cPassword: this.state.txtxContraseña,
            cEmail:this.state.txtCorreo,
            cName:this.state.txtNombre,
            nType: this.state.txtType.valor
        }
        fetch('http://35.229.89.115/temp/meetup/api/users/register', {
            method: 'POST',
            body: JSON.stringify(params),
            headers: {
              "Content-type": "application/json; charset=UTF-8"
            }
          })
        .then((response) => {
          return response.json()
        })
        .then((res) => {          
          if(res.respuesta){
            this.setState({ showAlert:true, typeAlert:"ok",  tituloAlert:"¡OK!", msjAlert:res.msj})
          }else{
            this.setState({ showAlert:true, typeAlert:"error",  tituloAlert:"¡Error!", msjAlert:res.error})
          }

        })
        .catch((err) => {
          this.setState({ showAlert:true, typeAlert:"error",  tituloAlert:"¡Espera!", msjAlert:"Error al contactar el servicio"})
      })
    }
    render(){
        return (
            <div>
                {/* style={styles.container} */}
                <div className="container">
                <Alert type={this.state.typeAlert} show={this.state.showAlert} titulo={this.state.tituloAlert} mensaje = {this.state.msjAlert} handleClose={this.hideAlert}></Alert>                       
                    <NavBar greeting={"Registrate"} name={""} isBack={true} handlerBack={this.backto} isLogout={false}/>
                    {/* style={styles.formBody} */}
                    <div className="formBody" >
                    {/* style={styles.headerForm} */}
                        <div className="headerForm">
                            <h2>
                                Danos tus datos
                            </h2>
                        </div>
                        {/* style={styles.body} */}
                        <div className="body" >
                            <AwesomeInput ide={"nombre"} label={"Nombre"} placeHolder={"ej. Juan Pérez"} onWrite={this.onChangeText}/>
                            <AwesomeInput ide={"correo"} label={"Correo"} placeHolder={"hola@empresa.com"} onWrite={this.onChangeText}/>
                            <AwesomeInput ide={"contrasena"} label={"Contraseña"} placeHolder={"******"} onWrite={this.onChangeText} isSecureKey={true}/>
                            <AwesomeInput ide={"tipo"} label={"Tipo de usuario"} data={this.state.tiposUSuarios} placeHolder={"Tipo"} type={"selected"} onWrite={this.onChangeText}/>
                        </div>
                        {/* style={styles.buttonsForm} */}
                        <div className="buttonsForm" >
                            <button style={{width:"100%", fontSize:17, padding: "8px 0px", marginTop:20, backgroundImage:"linear-gradient(to right, #a1c4fd 0%, #c2e9fb 51%, #a1c4fd 100%)", border:"none", outline:"none", borderRadius:5 }} onClick={() => this.registro()}>
                                Enviar
                            </button>
                        </div>
                        
                    </div>
                </div>
            </div>
        );
    }
}

const styles = {
    container:{
        position: "absolute",
        flex: 1,
        width:"100%",
    },
    formBody:{
        margin: '5% auto 0px auto',
        width:"40%",
        borderRadius: 10,
        border:"1px solid #eee",
        padding: 10,
        paddingBottom: 20,        

    },
    headerForm:{
        padding: "16px 16px",
        borderBottom:"1px solid #eee",
    },
    buttonsForm:{
        padding: "0px 16px",
    }
}

export default Login;
