import React from 'react';

import NavBar from '../components/navBar'
import DashboardAdmin from '../components/admin'
import DashboardUser from '../components/user'

const userGbl = JSON.parse(localStorage.getItem('myData'))

class Home extends React.Component {

  constructor(props){
    super()

    this.state = {
      user:{}
    }
    this.logOut = this.logOut.bind(this)

    // if( userGbl  === null){
    //   props.history.push("/")
    // }else{
    //   props.history.push("/home")
    // }

    let h = JSON.parse(localStorage.getItem('myData'))
    if(h !== null){
      console.log("LOAD", ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
      this.setState({user:h})
      props.history.push("/home")
    }else{
      props.history.push("/")
      console.log("ELSE LOAD", ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    }
  }
  
  logOut(){
    localStorage.removeItem('myData');
    localStorage.clear();
    this.props.history.push("/")
  }
    render(){
      return (
        <div>
            <NavBar greeting={"Bienvenido"} name={this.state.user.name} isLogout={true} handlerLogout={this.logOut}/>
            {
             this.state.user.type === 0?
              <DashboardAdmin/>:
              <DashboardUser/>
            }
                
        </div>
      );
    }
  }


export default Home;
