import React, { Component } from 'react';
import Calendar from 'react-calendar';
import {  FaCaretDown } from 'react-icons/fa'
import TimeField from 'react-simple-timefield';
import {Animated} from "react-animated-css";


class AwesomeInput extends Component {

    constructor(props) {
        super();
     
        this.state = {
            currentDate: new Date(),
            showResults: false,
            showResultsSelect:false,
            valueInputText:"",
            valueInputCalendar : "",
            valueInputHour:"",
            valueInputSelect:"",
            time: '12:00',
            array:[] //props.data !== "sele" ? 
        };
        this.onSelected = this.onSelected.bind(this)
      }

      componentDidMount(){
         
        let date =  new Date()
         let formatted_date = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear()

         if( this.props.type === "calendar" && this.props.value !== "" && this.props.value !== undefined ){
            this.setState({valueInputCalendar: this.props.value})
         }else{
            this.setState({valueInputCalendar: formatted_date})
         }

         if(this.props.type === "hour" && this.props.value !== "" && this.props.value !== undefined ){
            this.setState({time: this.props.value})
         }else{
            this.setState({time: "00:00"})
        }
        if(this.props.type === "text" && this.props.value !== "" && this.props.value !== undefined ){
            this.setState({valueInputText: this.props.value})
         }else{
            this.setState({valueInputText: ""})
        }
        if(this.props.type === "selected" && this.props.data !== undefined){
            this.setState({array:this.props.data})
        }
          
      }

      onChange = currentDate => {
        let formatted_date = currentDate.getDate() + "-" + (currentDate.getMonth() + 1) + "-" + currentDate.getFullYear()
        this.setState({ currentDate , showResults:false, valueInputCalendar: formatted_date})

        this.props.onWrite(formatted_date, this.props.ide)
      }
      
      onSelected(item){
          this.setState({valueInputSelect:item.texto, showResultsSelect: false})
          this.props.onWrite(item, this.props.ide)
      }
  render() {
   
    const listItems = this.state.array.map((item) =>
    <ListItem value={item} itemEdit={this.itemEdit} selectedItem={this.onSelected} itemDelete={this.itemDelete}/>)
   
    return (
        <div>  
            {
                this.props.isSecureKey === undefined || !this.props.isSecureKey?
                this.props.type === undefined || this.props.type === "" || this.props.type === "text" ?
                    <div style={{marginTop:10}}>
                        <label style={{fontSize:17}}> {this.props.label} </label>
                        <input value={this.state.valueInputText}  type="text" style={styles.baseInput} placeholder={this.props.placeHolder} onChange={(evt) => {
                            let value = evt.target.value
                            this.setState({ valueInputText: value })
                            this.props.onWrite(value, this.props.ide)
                        }} />
                    </div>
                    :
                <div></div>
                :
                <div style={{marginTop:10}}>
                    <label style={{fontSize:17}}> {this.props.label} </label>
                    <input value={this.state.valueInputText}  type="password" style={styles.baseInput} placeholder={this.props.placeHolder} onChange={(evt) => {
                        let value = evt.target.value
                        this.setState({ valueInputText: value })
                        this.props.onWrite(value, this.props.ide)
                    }} />
                </div>
            }          

            {
                this.props.type === "calendar" ?
                    <div style={{ borderBottom:"1px solid #eee", marginTop:10}}>
                        <label style={{fontSize:17}}> {this.props.label} </label>
                        <div style={{display:"flex", flexDirection:"row"}}>
                            <div style={{display:"flex", flexDirection:"column", width:"95%"}}>
                                <input disabled type="text" value={ this.state.valueInputCalendar} style={styles.baseInputCalendar}/>
                            </div>
                            <div style={{display:"flex", flexDirection:"row", width:"5%"}}>
                                <button style={{backgroundColor:"white", border:"none", outline:"none", width:"100%", fontSize:"1.1em"}} onClick={() => {
                                    this.setState({showResults: !this.state.showResults})
                                }}>
                                    <FaCaretDown/>
                                </button>
                            </div>
                            
                        </div>
                        <Animated animationIn="fadeInDown" animationOut="fadeOutUp" animationInDuration={1000} animationOutDuration={1000} isVisible={this.state.showResults}>
                            {this.state.showResults ? <Calendar style={{width:"100%", border:"none" }} onChange={this.onChange} value={this.state.currentDate} /> : null}
                        </Animated>

                    </div>
                 :
                <div></div>
                
            }    
            {
                this.props.type === "selected" ?
                    <div style={{ borderBottom:"1px solid #eee", margin:"10px 8px 0px 8px",}}>
                        <label style={{fontSize:17}}> {this.props.label} </label>
                        <div style={{display:"flex", flexDirection:"row"}}>
                            <div style={{display:"flex", flexDirection:"column", width:"95%"}}>
                                <input value={this.state.valueInputSelect} placeholder={"Selecciona una opcion..."} disabled type="text" style={styles.baseInputCalendar}/>
                            </div>
                            <div style={{display:"flex", flexDirection:"row", width:"5%"}}>
                                <button style={{backgroundColor:"white", border:"none", outline:"none", width:"100%", fontSize:"1.1em"}} onClick={() => {
                                    this.setState({showResultsSelect: !this.state.showResultsSelect})
                                }}>
                                    <FaCaretDown/>
                                </button>
                            </div>
                            
                        </div>
                        <Animated animationIn="fadeInDown" animationOut="fadeOutUp" animationInDuration={800} animationOutDuration={800} isVisible={this.state.showResultsSelect}>
                        {this.state.showResultsSelect ? 
                        <div style={{width:"100%", border:"1px solid #eee", borderRadius:10, boxShadow: "5px 5px 10px rgba(0,0,0, 0.1)"}}>
                            <ul> {listItems} </ul>
                        </div> : null}
                        </Animated>
                        
                        
                    </div>
                 :
                <div></div>
                
            }
            {
                this.props.type === "hour"?
                <div style={{marginTop:10}}>
                    <label style={{fontSize:17}}> {this.props.label} </label>
                    <TimeField value={this.state.time} style={styles.baseInput} colon=":" onChange={(time) => {
                        let value = time.target.value
                         this.props.onWrite(value, this.props.ide)
                     }} />
                </div>
                :
                <div></div>
            }
           
           
        </div>
    )
  }
}

const styles = {
    conteiner:{
        marginTop: 20,
    },
    baseInput:{
        width:"98%",
        height:40,
        outline:"none",
        border:"none",
        borderBottom:"1px solid #eee",
        fontSize:17,
        margin: '0px 8px',
    },
    baseInputCalendar:{
        width:"98%",
        height:40,
        outline:"none",
        border:"none",
        borderBottom:"none",
        fontSize:17,
        margin: '0px 8px',
    }
    
}

function ListItem(props) {
    const value = props.value;
    return (
    <li style={{listStyleType:"none"}} key={value.id}>
        <div style={{ padding: "15px 16px",  margin:"0px 10px", borderBottom:"1px solid #eee",  display:"block"}}>
            <button style={{width:"100%", height:"100%", textAlign:"left", backgroundColor:"white", border:"none", outline:"none", fontSize:17 }} onClick={() => props.selectedItem(value)}>
                {value.texto}
            </button>
        </div>
      </li>
    );
  }


export default AwesomeInput