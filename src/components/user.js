import React, { Component } from 'react';
import Modal from "../Helpers/Modal"
import CustomItemUser from '../Helpers/lisItemEventsUser'
import Alert from "../Helpers/Alert"


const userGbl = JSON.parse(localStorage.getItem('myData'))

class DashboardUser extends Component {

    constructor(props) {
        super();
     
        this.state = {
            show: false,
            itemSelect:{},
            array:[],
            arrayUserEvents:[],
            typeAlert:"",
            showAlert:false,
            tituloAlert:"",
            msjAlert:""
        };
     
        this.itemSelected = this.itemSelected.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.hideAlert = this.hideAlert.bind(this);
        // this.closeModal = this.closeModal.bind(this);
      }

      componentDidMount() {        
        this.getEvents()
      }
      hideAlert(){
        this.setState({showAlert:false})
    }
      getEvents(){
        fetch('http://35.229.89.115/temp/meetup/api/events/all')
        .then((response) => {
          return response.json()
        })
        .then((res) => {
          this.setState({array:res.data})
          this.getUserEvents()
        })
        .catch((err) => {
          this.setState({ showAdd:false, showAlert:true, typeAlert:"error",  tituloAlert:"¡Espera!", msjAlert:"Error al contactar el servicio"})
      })
      }

      getUserEvents(){
          let params = {
            "idUser":userGbl.id
        }
        fetch('http://35.229.89.115/temp/meetup/api/users/getUserEvents', {
            method: 'POST',
            body: JSON.stringify(params),
            headers: {
              "Content-type": "application/json; charset=UTF-8"
            }
          })
        .then((response) => {
          return response.json()
        })
        .then((res) => {
          this.setState({arrayUserEvents:res.data})

          let eventsUser = res.data
          let events = this.state.array
          let newEvents = []
          
          for (let i = 0; i < events.length; i++) {
            const element = events[i];
            element.enroll = false
            newEvents.push(element)
            for (let j = 0; j < eventsUser.length; j++) {
                const elementj = eventsUser[j];
                
                if(element.id === elementj.id_event){
                    element.enroll = true
                }
            }
          }
          
          this.setState({array:newEvents})

        })
        .catch((err) => {
          this.setState({ showAdd:false, showAlert:true, typeAlert:"error",  tituloAlert:"¡Espera!", msjAlert:"Error al contactar el servicio"})
      })
      }

      showModal = () => {
        this.setState({ show: true });
      };
    
      hideModal = () => {
        this.setState({ show: false });
      };

      itemSelected(item){

        let params = {
            "nIdUser": parseInt(userGbl.id),
            "nIdEvent":item.id
        }
        fetch('http://35.229.89.115/temp/meetup/api/events/enroll', {
            method: 'POST',
            body: JSON.stringify(params),
            headers: {
              "Content-type": "application/json; charset=UTF-8"
            }
          })
        .then((response) => {
          return response.json()
        })
        .then((res) => {
          if(res.respuesta){
          let events = this.state.array
          let newEvents = []
         
          
          for (let i = 0; i < events.length; i++) {
            const element = events[i];
            if(element.id === res.data.id_event){
                element.enroll = true
            }
            newEvents.push(element)
          }
          
          this.setState({array:newEvents})
        }else{
          this.setState({ showAdd:false, showAlert:true, typeAlert:"error",  tituloAlert:"¡Espera!", msjAlert:res.error})
        }

        })
        .catch((err) => {
          this.setState({ showAdd:false, showAlert:true, typeAlert:"error",  tituloAlert:"¡Espera!", msjAlert:"Error al contactar el servicio"})
      })
      }

      
  render() {
    // You can use them as regular CSS styles
    const listItems = this.state.array.map((item) =>
    <CustomItemUser value={item} itemSelected={this.itemSelected} />
    );
   
    return (
        <div>
          <Alert type={this.state.typeAlert} show={this.state.showAlert} titulo={this.state.tituloAlert} mensaje = {this.state.msjAlert} handleClose={this.hideAlert}></Alert>                       
            <Modal show={this.state.show} btnCancel={false}  btnAcept={false} titulo={this.state.itemSelect.titulo} handleClose={this.hideModal}>
                <div style={{display:"flex", flexDirection: 'row', width:"100%"}}>
                    <div style={{ fontSize:17, fontWeight:"600", display:"flex", flexDirection: 'column', width:"20%"}}>
                        Ponente
                    </div>
                    <div style={{ fontSize:17, fontWeight:"100", display:"flex", flexDirection: 'column',  width:"80%"}}>
                        {this.state.itemSelect.sPonente}
                    </div>
                </div>
                <div style={{display:"flex", flexDirection: 'row', width:"100%"}}>
                    <div style={{ fontSize:17, fontWeight:"600",display:"flex", flexDirection: 'column',  width:"25%"}}>
                        Hora:
                    </div>
                    <div style={{ fontSize:17, fontWeight:"100", display:"flex", flexDirection: 'column',  width:"25%"}}>
                        {this.state.itemSelect.sHora}
                    </div>
                    
                </div>

                <button style={{backgroundImage: "linear-gradient(to right, #a1c4fd 0%, #c2e9fb 51%, #a1c4fd 100%)", color:"white", fontSize:17, fontWeight:"bold", width:"100%", border:"none", marginTop:20, height:40, borderRadius:5,  outline:"none"}}>
                    Enrolarme 
                </button>
                
            </Modal>
            <div style={styles.conteiner}>
                <ul>
                    {listItems}
                </ul>

               
            </div>
        </div>
    )
  }
}



const styles = {
    conteiner:{
        marginTop: 20,
    },
    modal :{
        position: "fixed",
        top: 0,
        left: 0,
        width:"100%",
        height: "100%",
        background: "rgba(0, 0, 0, 0.6)",
      },
      modalMain:{
        position:"fixed",
        background: "white,",
        width: "80%",
        height: "auto",
        top:"50%",
        left:"50%",
        transform: "translate(-50%,-50%)",
      },
      displayBlock:{
        display: "block",
      },
      displayNone:{
        display: "none"
      }
    
}
export default DashboardUser