import React, { Component } from 'react';
import Modal from "../Helpers/Modal"
import Alert from "../Helpers/Alert"
import { FaTimes, FaPen, FaPlus } from 'react-icons/fa'
import HeaderTable from '../components/headerTable'
import AwesomeInput from '../components/comInput'
import CustomItemAdmin from '../Helpers/lisItemEventsAdmin'

class DashboardAdmin extends Component {

    constructor(props) {
        super();
     
        this.state = {
            showDelete: false,
            showAdd:false,
            showAlert:false,
            showEdit:false,
            itemSelect:{},
            itemSelectAdd:{},
            tituloAlert:"",
            msjAlert:"",
            typeAlert:"",
            txtTema:"",
            txtDireccion:"",
            txtHora:"",
            txtFecha:"",
            txtAgenda:"",
            txtPonente:"",
            array:[]
        };
     
        this.itemDelete = this.itemDelete.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.handleOk = this.handleOk.bind(this)
        this.handleOkAdd = this.handleOkAdd.bind(this)
        this.handleOnChange = this.handleOnChange.bind(this);
        this.hideAlert = this.hideAlert.bind(this)
        this.itemEdit = this.itemEdit.bind(this)
        this.handleOkEdit = this.handleOkEdit.bind(this);
      }
      componentDidMount() {
        
        this.getEvents()
    
      }

      getEvents(){
        fetch('http://35.229.89.115/temp/meetup/api/events/all')
          .then((response) => {
            return response.json()
          })
          .then((res) => {
            this.setState({array:res.data})
          })
          .catch((err) => {
            this.setState({ showAdd:false, showAlert:true, typeAlert:"error",  tituloAlert:"¡Espera!", msjAlert:"Error al contactar el servicio"})
        })
      }
      
      hideModal(ide){
          if(ide === "add"){
            this.setState({ showAdd: false });
          }else if(ide === "edit"){
            this.setState({ showEdit: false });
          }else{
            this.setState({ showDelete: false });
          }
      };
      hideAlert(){
          this.setState({showAlert:false})
      }

      itemDelete(item){
        this.setState({ showDelete: true, itemSelectAdd: item });
      }
      itemEdit(item){
        this.setState({ showEdit: true, itemSelect: item });
      }
      handleOk(item){

        let body = {
            "nId":item.id,
        }

        fetch('http://35.229.89.115/temp/meetup/api/events/delete', {
                method: 'POST',
                body: JSON.stringify(body),
                headers: {
                  "Content-type": "application/json; charset=UTF-8"
                }
              })
            .then((response) => {
                return response.json()
            })
            .then((res) => {
                if(res.respuesta){
                    var cont = 0

                    for (let i = 0; i < this.state.array.length; i++) {
                        const element = this.state.array[i];
                        
                        if(element.id === item.id){
                            cont = i
                        }
                    }
                    
                    this.state.array.splice(cont, 1)
                    this.setState({ showDelete: false, showAdd:false, showAlert:true, tituloAlert:"¡OK!", msjAlert: res.msj});
                }else{
                    this.setState({ showAdd:false, showAlert:true, typeAlert:"error",  tituloAlert:"¡Espera!", msjAlert:res.msj})
                }
            })
            .catch((err) => {
                this.setState({ showAdd:false, showAlert:true, typeAlert:"error",  tituloAlert:"¡Espera!", msjAlert:"Error al contactar el servicio"})
            })

      }
      handleOnChange(value, ide){
          if(ide=== "tema"){
            this.setState({txtTema:value})
          }else if(ide === "lugar"){
            this.setState({txtDireccion:value})
          }else if(ide === "hora"){
            this.setState({txtHora:value})
          }else if(ide==="agenda"){
            this.setState({txtAgenda:value})
          }else if(ide==="fecha"){
            this.setState({txtFecha:value})
          }else if(ide === "nombre"){
            this.setState({txtPonente:value})
          }
      }
      handleOkAdd(item){
        let tema = this.state.txtTema
        let direccion = this.state.txtDireccion
        let fecha = this.state.txtFecha
        let hora = this.state.txtHora
        let agenda = this.state.txtAgenda
        let ponente = this.state.txtPonente
     
        let body = {
            "sTema":tema,
            "sFecha":fecha,
            "sHora":hora,
            "sDireccion":direccion,
            "sAgenda":agenda,
            "sPonente":ponente
        }
        if( tema !== "" && direccion !== "" && fecha !== "" && hora !== "" && agenda !== ""){
            
            fetch('http://35.229.89.115/temp/meetup/api/events/create', {
                method: 'POST',
                body: JSON.stringify(body),
                headers: {
                  "Content-type": "application/json; charset=UTF-8"
                }
              })
            .then((response) => {
                return response.json()
            })
            .then((res) => {
                if(res.respuesta){
                    this.state.array.push(res.data)
                    this.setState({ showAdd:false, showAlert:true, tituloAlert:"¡OK!", msjAlert: res.msj})
                }else{
                    this.setState({ showAdd:false, showAlert:true, typeAlert:"error",  tituloAlert:"¡Espera!", msjAlert:res.msj})
                }
            })
            .catch((err) => {
                this.setState({ showAdd:false, showAlert:true, typeAlert:"error",  tituloAlert:"¡Espera!", msjAlert:"Error al contactar el servicio"})
            })
            
        }else{
            this.setState({ showAdd:false, showAlert:true, typeAlert:"warning", tituloAlert:"¡Espera!", msjAlert:"Faltan datos que llenar"})
        }
      }
      handleOkEdit(item){
        let tema = this.state.txtTema
        let direccion = this.state.txtDireccion
        let fecha = this.state.txtFecha
        let hora = this.state.txtHora
        let agenda = this.state.txtAgenda
        let ponente = this.state.txtPonente


        if(tema === ""){
          tema = this.state.itemSelect.sTema
        }
        if(direccion === ""){
          direccion = this.state.itemSelect.sDireccion
        }
        if(fecha === ""){
          fecha = this.state.itemSelect.sFecha
        }
        if(hora === ""){
          hora = this.state.itemSelect.sHora
        }
        if(agenda === ""){
          agenda = this.state.itemSelect.sAgenda
        }
        if(ponente === ""){
          ponente = this.state.itemSelect.sPonente
        }
     
        let body = {
            "nId":item.id,
            "sTema":tema,
            "sFecha":fecha,
            "sHora":hora,
            "sDireccion":direccion,
            "sAgenda":agenda,
            "sPonente":ponente
        }
        if( tema !== "" && direccion !== "" && fecha !== "" && hora !== "" && agenda !== ""  ){
            
            fetch('http://35.229.89.115/temp/meetup/api/events/edit', {
                method: 'POST',
                body: JSON.stringify(body),
                headers: {
                  "Content-type": "application/json; charset=UTF-8"
                }
              })
            .then((response) => {
                return response.json()
            })
            .then((res) => {
                if(res.respuesta){
                    this.getEvents()
                    this.setState({ showEdit:false, showAlert:true, tituloAlert:"¡OK!", msjAlert: res.msj})
                }else{
                    this.setState({ showEdit:false, showAlert:true, typeAlert:"error",  tituloAlert:"¡Espera!", msjAlert:res.msj})
                }
            })
            .catch((err) => {
                this.setState({ showEdit:false, showAlert:true, typeAlert:"error",  tituloAlert:"¡Espera!", msjAlert:"Error al contactar el servicio"})
            })
            
        }else{
            this.setState({ showEdit:false, showAlert:true, typeAlert:"warning", tituloAlert:"¡Espera!", msjAlert:"Faltan datos que llenar"})
        }
      }

     
  render() {
    // You can use them as regular CSS styles
    
    const listItems = this.state.array.map((item) =>
    <CustomItemAdmin value={item} itemEdit={this.itemEdit} itemDelete={this.itemDelete} />
    )
   
    return (
        <div>
            {/* display: props.show  !== undefined && props.show ? "block" : "none" */}
            
            <Alert type={this.state.typeAlert} show={this.state.showAlert} titulo={this.state.tituloAlert} mensaje = {this.state.msjAlert} handleClose={this.hideAlert}></Alert>                       
            <Modal show={this.state.showDelete} itemSelected={this.state.itemSelectAdd}  titulo={""} handleOk={this.handleOk} handleClose={this.hideModal}>
                <div>
                    <h2 style={{textAlign:"center"}}>¿Seguro deseas eliminar?</h2>
                    <div style={{fontSize:"2em", textAlign:"center", marginTop:15, color:"red"}}>
                        <FaTimes/>
                    </div>
                    
                </div>
            </Modal>
            <Modal show={this.state.showAdd} titulo={"Agregar Evento"} handleOk={this.handleOkAdd} ide={"add"} handleClose={this.hideModal}>
                <div>
                    <AwesomeInput type="text" label={"Tema"} placeHolder={"ej. Bigban Theory"} onWrite={this.handleOnChange} ide={"tema"}/>
                    <AwesomeInput type="text" label={"Ponente"} placeHolder={"ej. Jose Lopez"} onWrite={this.handleOnChange} ide={"nombre"}/>
                    <AwesomeInput type="text" label={"Direccion"} placeHolder={"ej. In the some place"} onWrite={this.handleOnChange} ide={"lugar"}/>
                    <div style={{display:"flex", flexDirection:"row"}}>
                        <div style={{display:"flex", paddingRight:2, flexDirection:"column", width:"48%", }}>
                            <AwesomeInput type="hour" label={"Hora"} onWrite={this.handleOnChange} ide={"hora"}/>
                        </div>
                        <div style={{display:"flex", paddingLeft:2, flexDirection:"column",  width:"48%", }}>
                        <AwesomeInput type="calendar" label={"Fecha"} onWrite={this.handleOnChange} ide={"fecha"}/>
                        </div>
                    </div>
                    <AwesomeInput type="text" label={""} placeHolder={"Agenda?"} onWrite={this.handleOnChange} ide={"agenda"}/>
                    
                </div>
            </Modal>

            <Modal show={this.state.showEdit} itemSelected={this.state.itemSelect} titulo={"Editar Evento"} handleOk={this.handleOkEdit} ide={"edit"} handleClose={this.hideModal}>
                <div>
                    {
                    this.state.itemSelect.sTema !== undefined ?
                    <div>
                    <AwesomeInput value={this.state.itemSelect.sTema} type="text" label={"Tema"}  onWrite={this.handleOnChange} ide={"tema"}/>
                    <AwesomeInput value={this.state.itemSelect.sPonente} type="text" label={"Ponente"} onWrite={this.handleOnChange} ide={"nombre"}/>
                    <AwesomeInput value={this.state.itemSelect.sDireccion} type="text" label={"Direccion"} onWrite={this.handleOnChange} ide={"lugar"}/>
                    <div style={{display:"flex", flexDirection:"row"}}>
                        <div style={{display:"flex", paddingRight:2, flexDirection:"column", width:"48%", }}>
                            <AwesomeInput value={this.state.itemSelect.sHora} type="hour" label={"Hora"} onWrite={this.handleOnChange} ide={"hora"}/>
                        </div>
                        <div style={{display:"flex", paddingLeft:2, flexDirection:"column",  width:"48%", }}>
                        <AwesomeInput value={this.state.itemSelect.sFecha} type="calendar" label={"Fecha"} onWrite={this.handleOnChange} ide={"fecha"}/>
                        </div>
                    </div>
                    <AwesomeInput value={this.state.itemSelect.sAgenda} type="text" label={""} onWrite={this.handleOnChange} ide={"agenda"}/>
                    </div>
                    :
                    null
                    }
                </div>
            </Modal>



            <div style={styles.conteiner}>
                <div style={{ margin:"0px 20px", textAlign:"right"}}>
                    <button onClick={() => this.setState({showAdd:true})} style={{width:48, height:48, borderRadius:24, marginRight:"2%", color:"white", fontSize:"1.2em", border:"none", outline:"none", backgroundImage: "linear-gradient(to right, #84fab0 0%, #8fd3f4 51%, #84fab0 100%)"}}>
                        <FaPlus/>
                    </button>
                </div>
                <div style={{ margin:"0px 8px 10px 8px",}}>
                    <HeaderTable/>
                    {
                        this.state.array.length > 0 ? 
                        <ul> {listItems} </ul>:
                        <div style={{width:"100%", textAlign:"center", color:"#696969"}}> Sin registros por el momento :(</div>
                    }
                    
                </div>

               
            </div>
        </div>
    )
  }
}



const styles = {
    conteiner:{
        marginTop: 20,
    },
    modal :{
        position: "fixed",
        top: 0,
        left: 0,
        width:"100%",
        height: "100%",
        background: "rgba(0, 0, 0, 0.6)",
      },
      modalMain:{
        position:"fixed",
        background: "white,",
        width: "80%",
        height: "auto",
        top:"50%",
        left:"50%",
        transform: "translate(-50%,-50%)",
      },
      displayBlock:{
        display: "block",
      },
      displayNone:{
        display: "none"
      }
    
}
export default DashboardAdmin