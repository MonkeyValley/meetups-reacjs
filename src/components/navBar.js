import React, { Component } from 'react';
import {FaSignOutAlt, FaArrowLeft} from 'react-icons/fa'
import '../StyleSheet/navBar.css'
class NavBar extends Component {

    constructor(props) {
        super();
     
        this.state = {
            
        };
     
      }

      
  render() {
   
    return (
        <div>                
            <nav className="nav">
                <div style={{display:"flex", flexDirection:"row", width:"100%"}}>
                  <div style={{display:"flex", flexDirection:"column", width:"90%"}}>
                    <div style={{display:"flex", flexDirection:"row", width:"100%"}}>
                        <div style={{display: this.props.isBack === undefined || !this.props.isBack? "none":"flex", flexDirection:"column", }}> <button style={{height:"100%", width:32, marginRight:10, fontSize:17, outline:"none", backgroundColor:"white", border:"none"}} onClick={() => this.props.handlerBack()} ><FaArrowLeft/></button> </div>
                        <div style={{display:"flex", flexDirection:"column", }}><h1>{this.props.greeting}</h1> </div>
                        <div style={{display:"flex", flexDirection:"column", }}><h4 style={{padding: 8, color:"#2196f3"}}>{this.props.name}</h4></div>
                    </div>
                  </div>
                <div style={{display:"flex", flexDirection:"column", width:"10%"}}>
                  <button  style={{height:"100%", display: this.props.isLogout === undefined || this.props.isLogout? "block":"none",  color:"red", fontSize:14, backgroundColor:"white", border:"none", outline:"none"}} onClick={() => this.props.handlerLogout()}>
                  <FaSignOutAlt /> <label className="lableCloseSesion">Cerrar sesión</label>
                  </button>
                </div>
                </div>
            </nav>
        </div>
    )
  }
}



const styles = {
    nav:{
        padding: "15px 5px",
        backgroundColor:"white",
        boxShadow: "0px 10px 10px rgba(0,0,0, 0.1)"
      }
}
export default NavBar