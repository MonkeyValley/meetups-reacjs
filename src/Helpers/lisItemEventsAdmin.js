import React from 'react';
import { FaTimes, FaPen } from 'react-icons/fa'
import '../StyleSheet/itemAdmin.css'
function CustomItemAdmin(props) {
    const value = props.value;
    return (
    <li style={{listStyleType:"none"}} key={value.id}>
        <div className="conteiner">
            <div style={{ padding: "8px 16px", flexDirection:"row", display:"flex", borderRadius:10, }}>
                
                <div style={{ display:"flex", flexDirection:"column", height:"100%", width:"10%", }}>
                    {value.id}
                </div>
                <div style={{  display:"flex", flexDirection:"column", height:"100%",width:"25%", }}>
                    {value.sPonente}
                </div>            
                <div style={{  display:"flex", flexDirection:"column", height:"100%", width:"20%", }}>
                    {value.sTema}
                </div>  
                <div className="columnDateTime">
                    {value.sFecha} - {value.sHora}
                </div>  
                <div style={{ display:"flex", flexDirection:"column", textAlign:"right", height:"100%",  width:"35%", }}>
                    <div style={{display:"flex", flexDirection:"row",  textAlign:"right", width:"100%"}}>
                        <div style={{display:"flex", flexDirection:"row", width:"85%"}}>

                        </div>
                        <div style={{display:"flex", flexDirection:"row"}}>
                            <button onClick={() => props.itemDelete(value)} style={{color:"red", height:36, width:36, fontSize:"1.5em", backgroundColor:"white", border:"none", outline:"none"}}>
                                <FaTimes />
                            </button>
                        </div>
                        <div style={{display:"flex", flexDirection:"row"}}>
                            <button onClick={() => props.itemEdit(value)} style={{color:"orange", height:36, width:36, fontSize:"1.2em", backgroundColor:"white", border:"none", outline:"none"}}>
                                <FaPen/>
                            </button>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
      </li>
    );
  }

export default CustomItemAdmin