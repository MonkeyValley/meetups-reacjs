import React from 'react';
import { FaTimes } from 'react-icons/fa'
import {Animated} from "react-animated-css";
import '../StyleSheet/modal.css'


const Modal = props => {
    return (
    <div>
        <Animated animationIn="fadeIn" animationOut="fadeOut" animationInDuration={1000} animationOutDuration={1000} isVisible={props.show}>
        <div className="backModal" style={{display: props.show  !== undefined && props.show ? "block" : "none" , width:"100%", backgroundColor:"rgba(0,0,0,0.2)"}}>
            <div className="bodyModal" >
                <div style={{width:"100%", borderBottom:"1px solid #eee", padding: "10px 0px"}}>
                    <div style={{display:"flex", flexDirection: 'row', width:"100%"}}>
                        <div style={{ fontSize:17, fontWeight:"600", display:"flex", flexDirection: 'column', width:"90%"}}>
                            {props.titulo}
                        </div>
                        <div style={{ fontSize:17, fontWeight:"100", display:"flex", flexDirection: 'column',  width:"10%"}}>
                            <button onClick={() => props.handleClose(props.ide)} style={{border:"none", backgroundColor:"white", height:"100%", fontSize:"1.3em", fontWeight:"100", outline:"none"}}><FaTimes /></button>
                        </div>
                    </div>
                    
                </div>
                <div style={{marginTop:20}}>
                    {props.children}
                </div>
                <div style={{width:"100%", textAlign:"right", marginTop:20}}>
                    <button  onClick={() => props.handleClose(props.ide)} style={{display: props.btnCancel === undefined || props.btnCancel ? "inline-block":"none", padding:"8px 16px", marginRight:8, backgroundColor:"white", borderRadius:5, border:"0.5px solid #eee", color:"black", outline:"none"}}>Cancelar</button>
                    <button  onClick={() => props.handleOk(props.itemSelected)} style={{display: props.btnAcept === undefined || props.btnAcept ? "inline-block":"none", padding:"8px 16px", backgroundColor:"black", borderRadius:5, border:"none", color:"white", outline:"none"}}>Aceptar</button>
                </div>
            </div>
        </div>     
        </Animated>
    </div>
    );
  };

  export default Modal