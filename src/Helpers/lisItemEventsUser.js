import React from 'react';
import '../StyleSheet/itemUser.css'

function CustomItemUser(props) {
    const value = props.value;
    return (

    <div className="backItem">
        <div style={{ padding: "8px 16px", flexDirection:"row",  height:170,  borderRadius:10, }}>
            
            <div style={{ width:"100%", }}>
                <div style={{backgroundColor:"black", height:70,  width:70 , borderRadius:45,  margin:"auto", }}>
                    <h3 style={{color:"white", fontSize:"3em", textAlign:"center"}}> {value.sPonente[0]} </h3>
                </div>
            </div>
            <div style={{   width:"100%", }}>
                <h3 style={{ fontSize:"1.1em", textAlign:"center"}}> {value.sPonente} </h3>
                <div style={{overflow:"hidden", whiteSpace:"nowrap", textOverflow:"ellipsis", textAlign:"center" }}>
                    {value.sTema}
                </div>
                
                <div style={{overflow:"hidden", whiteSpace:"nowrap", textOverflow:"ellipsis", textAlign:"center" }}>
                    Hora: <label style={{color:"#2196f3"}}>{value.sHora}</label> Fecha: <label style={{color:"#2196f3"}}>{value.sFecha}</label>
                </div>
                
                
                    <button disabled ={ value.enroll ? true : false} onClick={() => props.itemSelected(value)} style={{ borderRadius:5, marginTop:8, padding: "3px 0px",  outline:"none", border:"none",  backgroundImage: value.enroll? "none" : "linear-gradient(to right, #a1c4fd 0%, #c2e9fb 51%, #a1c4fd 100%)", color:"#000", textAlign:"center", width:"100%", display:"block"}}>
                        {
                            value.enroll?
                            <div>Asistiré</div>:
                            <div>Asistir</div>
                        }
                    </button>
                    
                
            </div>            
        </div>
      </div>
      
    );
  }

  export default CustomItemUser