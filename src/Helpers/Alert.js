import React from 'react';
import { FaTimes } from 'react-icons/fa'
import {Animated} from "react-animated-css";
import '../StyleSheet/alert.css'

const Modal = props => {
    return (
    <div>
        <Animated animationIn="fadeIn" animationOut="fadeOut" animationInDuration={1000} animationOutDuration={1000} isVisible={props.show}>
        <div className="backAlert" style={{display: props.show  !== undefined && props.show ? "block" : "none",  }}>
            <div className="bodyAlert">
                <div style={{width:"100%", padding: "10px 0px", textAlign:"right"}}>
                    <button onClick={() => props.handleClose(props.ide)} style={{border:"none", backgroundColor:"white", height:"100%", fontSize:"1.3em", fontWeight:"100", outline:"none"}}><FaTimes /></button>
                </div>
                <div style={{marginTop:20, textAlign:"center"}}>
                    {
                        props.type === "warning" ? 
                        <img style={{width:"30%", margin:"0 auto"}} src={require('../Assets/warning.png')} alt="no_image" />:
                        props.type === "error" ?
                        <img style={{width:"30%", margin:"0 auto"}} src={require('../Assets/error.png')} alt="no_image" />:
                        <img style={{width:"30%", margin:"0 auto"}} src={require('../Assets/ok.png')} alt="no_image" />
                    }
                    
                    <h2 style={{marginTop:20}}>
                        {props.titulo}
                    </h2>
                    <p style={{fontWeight:"100"}}>
                        {props.mensaje}
                    </p>
                </div>
                <div style={{width:"100%", textAlign:"right", marginTop:20}}>
                </div>
            </div>
        </div>  
        </Animated>   
    </div>
    );
  };

  export default Modal