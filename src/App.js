import React from 'react';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";

import Login from './Views/Login.js'
import Home from './Views/Home.js'


function App() {
  
  return (
    <Router>
    
      <Route path="/" component={Login}/>
      <Route path="/home" component={Home}/> 
     
    </Router>
  );
}



export default App;
